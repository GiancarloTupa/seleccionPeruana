
$(window).on('scroll', function(){
	var scrollUsuario = $(window).scrollTop();

	//efecto a todos los elementos con clase reveal
	$('.reveal').each(function(){
		
		var profundidad = $(this).offset().top - $(window).innerHeight() / 1.05; //innerHeight para obtener la mitad de la pantalla
		

		if (scrollUsuario > profundidad) {
			$(this).addClass('visible');
		}
		
	});

});


$(document).ready(function(){

	$('html').scrollTop(0);

	barraMenu();
	
	//desplazar con el nav hacia las section
	desplazar();

	//botones modales
	botonModales();

	//validacion de formulario
	validacionFormulario();

	//TYPED
	typed();

	carrusel();


	
});



function barraMenu(){
	$('.barrasMenu').on('click', function(){
		$('.menu').toggleClass('abierto');
	});
}

function desplazar(){
	$('.desplazar').on('click', function(){
		
		
		var dataNivel = $($(this).data('nivel')).offset().top;

		if (innerWidth > 1070) {

			$('html').animate({
				scrollTop: dataNivel - 40
			}, 500);
		}

		if (innerWidth <= 1070) {

			$('html').animate({
				scrollTop: dataNivel
			}, 500);
			$('.menu').removeClass('abierto');
		}

	});
}

function botonModales(){
	var contenidoModal ="";
	$('.botonModal').on('click', function(){
		var boton = $(this).data('contenido');
		contenidoModal = $(this).data('nivel');
			$('.menu').fadeOut(300);
			$('.fondoModal').fadeIn(500, function(){
				$('.contenidoModal[data-contenido='+boton+']').fadeIn(500);
			});

			$('.cerrarModal').on('click', function(){
				$('.menu').fadeIn(300);
				$('.contenidoModal, .fondoModal').fadeOut(500);

	//			var dato = $(this).data('nivel');
	
				if (innerWidth >= 1039) {

					var dataNivel = $(contenidoModal).offset().top - 72;
						$('html').animate({
						scrollTop: dataNivel
					}, 100);
				}

				if (innerWidth <= 1038) {

					var dataNivel = $(contenidoModal).offset().top;
						$('html').animate({
						scrollTop: dataNivel
					}, 100);
				}
				
					
		});

	});
}

function validacionFormulario(){
	$('#formularioContacto').on('submit', function(e){
		var nombre = $('#userName').val();
		var email = $('#userEmail').val();
		var mensaje = $('#userMessage').val();

		if (nombre.length == 0) {
			mostrarError("El nombre no puede estar vacio");
			e.preventDefault();
		}else if (email.length == 0) {
			mostrarError("El email no puede estar vacio");
			e.preventDefault();
		}else if (email.includes("@") == false || email.includes(".") == false) {
			mostrarError("Te falta el @ o el '.(punto)' en el email");
			e.preventDefault();
		}
		else if (mensaje.length < 10) {
			mostrarError("El mensaje es muy corto");
			e.preventDefault();
		}	
	});


}

function mostrarError(texto){
		$('#alerta p').text(texto);
		$('#alerta').addClass('visible');
		setTimeout(function() {
			$('#alerta').removeClass('visible');
		}, 4000);
}

function carrusel(){
	$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:20,
	  	autoplay:true,
	    autoplayTimeout:3000,
	    smartSpeed:2000,
	    dots:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        430:{
	            items:1
	        },
	        620:{
	            items:2
	        },
	        1000:{
	            items:3
	        }
	    }
	});
}

function typed(){
	var options = {
	  strings: ["","Envia tus sugerencias, comentarios, lo que deseas saber de nuestra selección..."],
	  typeSpeed: 40,
	  backSpeed: 100,
	  fadeOut: true,
	  loop: true,
	}

	var typed = new Typed(".mail", options);	
}