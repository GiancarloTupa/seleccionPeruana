<?php
if($_POST)
{
    $to_Email       = "gtupavela@gmail.com"; // email de recepción
    $subject        = 'SUGERENCIAS'; // asunto

    // saneado de seguridad
    $user_Name        = filter_var($_POST["userName"], FILTER_SANITIZE_STRING);
    $user_Email       = filter_var($_POST["userEmail"], FILTER_SANITIZE_EMAIL);
    $user_Message     = filter_var($_POST["userMessage"], FILTER_SANITIZE_STRING);
    
    // composición de email
    $headers = "MIME-Version: 1.0";
    $headers .= "Content-type: text/html; charset=charset=UTF-8";
    $headers .= 'From: SUGERENCIAS' . "\r\n" .
    'Reply-To: '.$user_Email.'' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    $body = "Se ha recibido una nueva sugerencia:"."\r\n";
    $body .= "Nombre: " . $user_Name ."\r\n";
    $body .= "Correo: " . $user_Email ."\r\n";
    $body .= "Mensaje: " . $user_Message ."\r\n";

    
    // envío de email
    $sentMail = @mail($to_Email, $subject, $body, $headers);
    

    // redireccionamiento tras envío
    if($sentMail)
    {
        header('Location: index.html'); 
    }
}
?>